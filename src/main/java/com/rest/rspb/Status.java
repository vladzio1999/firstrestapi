package com.rest.rspb;

enum Status {
    IN_PROGRESS,
    COMPLETED,
    CANCELLED;
}
