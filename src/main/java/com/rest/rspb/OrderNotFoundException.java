package com.rest.rspb;

class OrderNotFoundException extends RuntimeException {

    OrderNotFoundException(Long id) {
        super("Could not find an order " + id);
    }
}
