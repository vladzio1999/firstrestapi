package com.rest.rspb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RspbApplication {

	public static void main(String[] args) {
		SpringApplication.run(RspbApplication.class, args);
	}

}
