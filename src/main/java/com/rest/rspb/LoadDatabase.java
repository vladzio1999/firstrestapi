package com.rest.rspb;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.logging.*;

@Configuration
class LoadDatabase {

    private static Logger logger = Logger.getLogger(LoadDatabase.class.getName());

    @Bean
    CommandLineRunner initDatabase(EmployeeRepository repository, OrderRepository orderRepository) {
        return args -> {
            logger.info("Preloading " + repository.save(new Employee("Bilbo", "Baggins", "burglar")));
            logger.info("Preloading " + repository.save(new Employee("Frodo", "Baggins", "thief")));

            orderRepository.save(new Order("MacBook Pro", Status.COMPLETED));
            orderRepository.save(new Order("iPhone", Status.IN_PROGRESS));

            orderRepository.findAll().forEach(order -> {
                logger.info("Preloaded " + order);
            });
        };
    }
}
